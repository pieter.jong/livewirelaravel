<?php

namespace App\Http\Livewire;

use Carbon\Carbon;
use Livewire\Component;
use Carbon\Traits\Date;

class Counter extends Component
{

    public $count;

    public $search = 'Hello world';

    public $page = 1;

    //Todo deze begrijp ik nog niet helemaal: Casting Properties @ https://laravel-livewire.com/docs/properties
//    protected $casts = [
//        'count' => 'Date'
//    ];

    protected $updatesQueryString = [
        'search' => ['except' => ''],
        'page',
    ];

    public function mount()
    {
        $this->count = (new Carbon())->format('d-m-Y');
//        $this->search = request()->query('search', $this->search);
        $this->fill(request()->only('search', 'page'));
    }

    public function increment()
    {
        $this->count = (new Carbon($this->count))   ->addDays(1)->format('d-m-Y');
    }

    public function decrement()
    {
        $this->count = (new Carbon($this->count))->subDays(1)->format('d-m-Y');;
    }

    public function render()
    {
        return view('livewire.counter');
    }
}
