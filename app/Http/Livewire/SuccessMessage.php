<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SuccessMessage extends Component
{
    public $successMessage = false;
    public $failMessage = false;

    protected $listeners = ['successMessage'];

    public function successMessage(): void
    {
        $this->successMessage = true;
    }

    public function render()
    {
        return view('livewire.success-message');
    }

}
