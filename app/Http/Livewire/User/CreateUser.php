<?php

namespace App\Http\Livewire\User;

use App\User;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class CreateUser extends Component
{
    public $name;
    public $email;
    public $password;
    public $passwordConfirmation;

    public function rules()
    {
        return [
            'name' => 'required|min:3',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6|same:passwordConfirmation',
        ];
    }

    public function storeUser(): void
    {
        $data = $this->validate();

        User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $this->emit('listUser');
        $this->reset();
    }

    public function render()
    {
        return view('livewire.user.create-user');
    }
}
