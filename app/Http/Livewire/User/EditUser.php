<?php

namespace App\Http\Livewire\User;

use App\User;
use Livewire\Component;

class EditUser extends Component
{
    public User $user;

    public function rules()
    {
        return [
            'user.name' => 'required|min:3',
            'user.email' => 'required|email'
        ];
    }

    protected $listeners = ['editUser' => 'editUser'];

    public function editUser(User $user): void
    {
        $this->user = $user;
    }

    public function updateUser(): void
    {
        $this->validate();
        $this->user->save();

        $this->emit('listUser');
        $this->emit('successMessage');
    }

    public function render()
    {
        return view('livewire.user.edit-user');
    }
}
