<?php

namespace App\Http\Livewire\User;

use App\User;
use Illuminate\Support\Arr;
use Livewire\Component;

class ListUsers extends Component
{
    protected $listeners = ['listUser' => 'render', 'deleteUser' => 'deleteUser'];

    public function editUser(User $user): void
    {
        $this->emit('editUser', Arr::only($user->toArray(), ['id']));
    }

    public function deleteUser(User $user)
    {
        $user->delete();
        $this->emit('listUser');
    }

    public function render()
    {
        return view('livewire.user.list-users')
            ->with('users', User::all());
    }
}
