<div style="text-align: center">
    <div class="row mt-5">
        <div class="col-12">
            <input wire:model="search" type="text" placeholder="Search posts by title...">
        </div>
        <div class="col-12 mt-2">
            <h1>Search Results:</h1>
            <h1>{{ $search }}</h1>
        </div>
    </div>
    <div class="col-12">
        <button class="btn btn-primary" wire:click="increment">+</button>
        <h1>{{ $count }}</h1>
        <button class="btn btn-danger" wire:click="decrement">-</button>
    </div>


</div>








