<div>
    @if ($successMessage)
        <div class="fixed-top">
            @if ($successMessage)
                <div wire:model="successMessage" class="alert alert-success" role="alert">
                    <i class="far fa-check-circle fa-lg mr-2"></i><span class="lead"><strong>successvol opgeslagen</strong></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><span class="small"><i class="fas fa-times"></i></span></span>
                    </button>
                </div>
            @endif
            @if ($failMessage)
                <div class="alert alert-danger" role="alert">
                    <i class="fas fa-exclamation-circle fa-lg mr-2"></i><span
                            class="lead"><strong>successvol opgeslagen</strong></span>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><span class="small"><i class="fas fa-times"></i></span></span>
                    </button>
                </div>
            @endif
        </div>
    @endif
</div>
