{{--{{$success}}--}}
<div>
    <div class="form-group">
        <label for="exampleInputEmail1">Name</label>
        <input wire:model.defer="name" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
        @error('name')
        <div class="mt-1 text-red-500 text-sm">{{$message}}</div> @enderror
    </div>

    <div class="form-group">
        <label for="exampleInputEmail1">Email address</label>
        <input wire:model.defer="email" type="email" class="form-control" id="exampleInputEmail1"
               aria-describedby="emailHelp">
        @error('email')
        <div class="mt-1 text-red-500 text-sm">{{$message}}</div> @enderror
        <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
    </div>

    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input wire:model.defer="password" type="password" class="form-control" id="exampleInputPassword1">
        @error('password')
        <div class="mt-1 text-red-500 text-sm">{{$message}}</div> @enderror
    </div>

    <div class="form-group">
        <label for="passwordConfirmation">Confirm Password</label>
        <input wire:model.defer="passwordConfirmation" id="passwordConfirmation" type="password" required class="form-control"/>
    </div>
    <button wire:click="storeUser" type="submit" class="btn btn-primary mt-3">Submit</button>
</div>
