<div>
    <div class="text-left mt-2">
        <div class="form-group">
            <label for="exampleInputEmail1">Name</label>
            <input wire:model.defer="user.name" type="text" class="form-control" id="exampleInputEmail1">
            @error('user.name')
                <div class="mt-1 text-red-500 text-sm">{{ $message }}</div>
            @enderror
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input wire:model.defer="user.email" type="email" class="form-control" id="exampleInputEmail1">
            @error('user.email')
                <div class="mt-1 text-red-500 text-sm">{{ $message }}</div>
            @enderror
        </div>
    </div>

    <div class="modal-footer">
        <button wire:click="updateUser" type="submit" class="btn btn-primary">
            <span wire:loading class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
            Opslaan

        </button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">
            Close
        </button>
    </div>
</div>