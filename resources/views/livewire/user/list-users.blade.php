<div class="text-left mt-5">
    <h3>All registerd users</h3>

    <table class="table table-hover table-striped table-sm">
    <thead>
    <tr>
        <th scope="col">Name</th>
        <th scope="col">E-mail</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    @foreach ($users as $user)
        <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td class="float-right">
                {{--                        <button class="btn btn-primary">Edit user</button>--}}
                <button wire:click="editUser({{ $user->id }})" type="button" class="btn btn-primary" data-toggle="modal"
                        data-target="#update_user">
                    <span wire:loading class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                    Edit User
                </button>

                <button class="btn btn-danger" onclick="confirm('Confirm delete?') && livewireDeleteEvent('deleteUser', {{$user->id}})">Delete</button>
            </td>
        </tr>
    @endforeach
    </tbody>
    </table>
    <script>
        //Todo deze functie kan in een algemeen script worden ingeladen
        function livewireDeleteEvent(event, id){
            window.livewire.emit(event, id);
        }
    </script>
</div>
