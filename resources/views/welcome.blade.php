<!DOCTYPE html>
<?php
/** @var \App\User $user */
/** @var \App\Post $post */
?>

<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LiveWire Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
@stack('styles')

<!-- Javascript. -->
    <script src="{{ asset('js/app.js') }}"></script>
    @stack('scripts')
    @livewireStyles
</head>

<body>

@livewire('success-message')

<div class="col-12">
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ route('login') }}">Login</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}">Register</a>
                @endif
            @endauth
        </div>
    @endif
    <div class="content">
        <div class="row">
            <div class=" col-12 title m-b-md">
                <h1>LiveWire <small class="text-muted">Laravel</small></h1>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item active"><a class="nav-link" href="https://laravel.com/docs">Docs</a>
                        </li>
                        <li class="nav-item"><a class="nav-link" href="https://laracasts.com">Laracasts</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://laravel-news.com">News</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://blog.laravel.com">Blog</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://nova.laravel.com">Nova</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://forge.laravel.com">Forge</a></li>
                        <li class="nav-item"><a class="nav-link" href="https://vapor.laravel.com">Vapor</a></li>
                        <li class="nav-item"><a class="nav-link"
                                                href="https://github.com/laravel/laravel">GitHub</a></li>
                    </ul>
                </div>
            </nav>
        </div>


        <div class="row">
            <div class="col-12">@livewire('user.list-users')</div>
            <x-modal id="update_user" title="Update user">
                @livewire('user.edit-user')
            </x-modal>
        </div>
    </div>

    <div class="row">
        <div class="col text-left mt-5">
            <a data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false"
               aria-controls="collapseExample"><h3>Register a user</h3></a>
            <div class="col-12">
                <div class="collapse" id="collapseExample">
                    @livewire('user.create-user')
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 mt-5">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                Launch demo modal
            </button>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    @livewire('counter')
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


@livewireScripts
</body>

<script>
    {{--    todo dit moet natuurlijk in een appart .js bestand--}}
    window.livewire.on('successMessage', message => {
        $('.modal').modal('hide');
        setTimeout(function () {
            $('.alert-success').fadeOut(1500);
        }, 1500);
    })
</script>

</html>
